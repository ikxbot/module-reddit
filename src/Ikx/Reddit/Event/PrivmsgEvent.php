<?php
namespace Ikx\Reddit\Event;

use Ikx\Core\Event\AbstractEvent;
use Ikx\Core\Event\EventInterface;
use Ikx\Core\Utils\MessagingTrait;

class PrivmsgEvent extends AbstractEvent implements EventInterface {
    use MessagingTrait;
    /**
     * Command executor
     */
    public function execute()
    {
        if ($this->channel) {
            foreach ($this->parts as $part) {
                if (substr($part, 0, 1) == ':') {
                    $part = substr($part, 1);
                }

                if (preg_match("/https?:\/\/(www\.)reddit\.com[^\s]+/m", $part)) {
                    $url = $part;
                    if (substr($url, -1, 0) == "/") {
                        $url = substr($url, 0, -1);
                    }

                    $url = $url . '.json';
                    $json = file_get_contents($url);
                    if ($json && $json = json_decode($json, true)) {
                        $post = $json[0]['data']['children'][0]['data'];
                        $subreddit = $post['subreddit_name_prefixed'];
                        $title = $post['title'];
                        $upvotePerc = $post['upvote_ratio'] * 100 . '%';
                        $score = $post['score'];
                        $url = $post['url'];
                        $author = $post['author'];
                        $comments = $post['num_comments'];
                        $created = (int)$post['created'];

                        $suffix = '';

                        if ($url != '' && $url != null) {
                            $suffix = ' (' . $url . ')';
                        }

                        $data = [];
                        $data[] = sprintf("Reddit: \x02%s\x02 on %s%s - %d upvotes (%s)", $title, $subreddit, $suffix, $score, $upvotePerc);
                        $data[] = sprintf("Created by %s on %s - %d comments", $author, date('Y-m-d H:i', $created), $comments);

                        foreach ($data as $line) {
                            $this->msg($this->channel, $line);
                        }
                    }
                }
            }
        }
    }
}