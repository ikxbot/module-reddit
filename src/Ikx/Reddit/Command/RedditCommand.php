<?php
namespace Ikx\Reddit\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Entity\Network;
use Ikx\Core\Utils\MessagingTrait;
use Ikx\Core\Utils\Format;

class RedditCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;


    /**
     * Command runner
     */
    public function run()
    {
        if (!count($this->params)) {
            $this->msg($this->channel, Format::bold("Usage"));
            $this->msg($this->channel, sprintf('%s RANDOM [subreddit] - Fetch a random thread from a subreddit',
                Network::getInstance()->get('prefix') . $this->command));
            $this->msg($this->channel, sprintf('%s LAST [subreddit] - Fetch the last thread from a subreddit',
                Network::getInstance()->get('prefix') . $this->command));
            $this->msg($this->channel, sprintf('%s <subreddit> - Fetch a random thread from a subreddit',
                Network::getInstance()->get('prefix') . $this->command));
        } else {
            switch(strtoupper($this->params[0])) {
                case 'RANDOM':
                    $this->random($this->params[1] ?? null);
                    break;
                case 'LAST':
                    $this->last($this->params[1] ?? null);
                    break;
                default:
                    $this->random($this->params[0]);
                    break;
            }
        }
    }

    private function random($subreddit = null) {
        $this->fetch('random', $subreddit);
    }

    private function last($subreddit = null) {
        $this->fetch('new', $subreddit);
    }

    private function fetch($type = 'random', $subreddit = null) {
        if ($subreddit == null) {
            $url = sprintf("https://api.reddit.com/%s", $type);
        } else {
            $url = sprintf("https://api.reddit.com/r/%s/%s", $subreddit, $type);
        }

        $ch = curl_init($url);
        curl_setopt_array($ch, [
            CURLOPT_USERAGENT           => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36',
            CURLOPT_RETURNTRANSFER      => true,
            CURLOPT_FOLLOWLOCATION      => true,
            CURLOPT_CONNECTTIMEOUT      => 3,
            CURLOPT_TIMEOUT             => 5
        ]);

        $contents = curl_exec($ch);

        if (isset($contents)) {
            $jsonObject = json_decode($contents);

            if (is_array($jsonObject) && count($jsonObject) || (
                isset($jsonObject->data) && count($jsonObject->data->children))) {
                if (isset($jsonObject->data)) {
                    $item = $jsonObject->data->children[0]->data;
                } else {
                    $item = $jsonObject[0]->data;
                }

                if (isset($item->children) && count($item->children)) {
                    $item = $item->children[0]->data;
                }

                $itemText  = $item->selftext;
                $itemTitle = $item->title;
                $itemUrl   = $item->url;

                $textLines = explode("\n", $itemText);
                $itemTextLines = [];

                $waitBetweenLines = 0;
                if (count($textLines) > 5) {
                    $waitBetweenLines = rand(5000, 50000);
                }

                foreach($textLines as $line) {
                    if (strlen($line) < 100) {
                        $itemTextLines[] = $line;
                    } else {
                        $itemSentences = explode('. ', $line);
                        foreach($itemSentences as $sentence) {
                            $itemTextLines[] = $sentence;
                            usleep($waitBetweenLines);
                        }
                    }
                }

                if (strlen($itemText) < 4) {
                    $this->msg($this->channel, $itemTitle . " - " . $itemUrl);
                } else {
                    $this->msg($this->channel, "\x02" . $itemTitle);

                    foreach($itemTextLines as $line) {
                        $this->msg($this->channel, $line);
                        usleep($waitBetweenLines);
                    }

                    $this->msg($this->channel, "\x0312" . chr(31) . $itemUrl);
                }

            } else {
                $this->msg($this->channel, 'No data returned from r/' . $subreddit);
            }
        } else {
            $this->server->log('Error found while fetching data from Reddit for r/' . $subreddit . ': [' . curl_errno($ch) . '] ' . curl_error($ch));
            $this->msg($this->channel,'Error: ' . curl_error($ch));
            $this->msg($this->channel,'The incident has been logged.');
        }

        curl_close($ch);
    }

    /**
     * Command describer (used for help)
     */
    public function describe()
    {
        return "Fetch a thread from a subreddit";
    }
}